[    0.000000] Linux version 6.6.0-38.0.0.46.oe2403.riscv64 (abuild@openeuler-riscv64) (gcc_old (GCC) 12.3.1 (openEuler 12.3.1-31.oe2403), GNU ld (GNU Binutils) 2.41) #1 SMP PREEMPT Fri Aug 16 18:07:42 UTC 2024
[    0.000000] Machine model: Sipeed Lichee Pi 4A
[    0.000000] SBI specification v2.0 detected
[    0.000000] SBI implementation ID=0x1 Version=0x10004
[    0.000000] SBI TIME extension detected
[    0.000000] SBI IPI extension detected
[    0.000000] SBI RFENCE extension detected
[    0.000000] earlycon: uart0 at MMIO32 0x000000ffe7014000 (options '115200n8')
[    0.000000] printk: bootconsole [uart0] enabled
[    0.000000] efi: EFI v2.7 by EDK II
[    0.000000] efi: MEMATTR=0x1ff4fb018 INITRD=0x1fdd07a18 MEMRESERVE=0x1fdd07a98 
[    0.000000] OF: reserved mem: OVERLAP DETECTED!
               memory@32000000 (0x0000000032000000--0x0000000038400000) overlaps with memory@32E00000 (0x0000000032e00000--0x0000000033400000)
[    0.000000] OF: reserved mem: 0x000000001e000000..0x000000001e00ffff (64 KiB) map non-reusable memory@1E000000
[    0.000000] OF: reserved mem: 0x0000000032000000..0x00000000383fffff (102400 KiB) map non-reusable memory@32000000
[    0.000000] OF: reserved mem: 0x0000000032e00000..0x00000000333fffff (6144 KiB) map non-reusable memory@32E00000
[    0.000000] OF: reserved mem: 0x0000000033400000..0x00000000335fffff (2048 KiB) map non-reusable memory@33400000
[    0.000000] OF: reserved mem: 0x0000000033600000..0x00000000337fffff (2048 KiB) map non-reusable memory@33600000
[    0.000000] cma: Reserved 64 MiB at 0x00000000fc000000 on node -1
[    0.000000] NUMA: No NUMA configuration found
[    0.000000] NUMA: Faking a node at [mem 0x0000000000a00000-0x00000001ffffffff]
[    0.000000] NUMA: NODE_DATA [mem 0x1ffde4c80-0x1ffde9fff]
[    0.000000] Zone ranges:
[    0.000000]   DMA32    [mem 0x0000000000a00000-0x00000000ffffffff]
[    0.000000]   Normal   [mem 0x0000000100000000-0x00000001ffffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000000a00000-0x00000000383fffff]
[    0.000000]   node   0: [mem 0x0000000038400000-0x00000000397fffff]
[    0.000000]   node   0: [mem 0x0000000039800000-0x00000001ffe3cfff]
[    0.000000]   node   0: [mem 0x00000001ffe3d000-0x00000001ffffefff]
[    0.000000]   node   0: [mem 0x00000001fffff000-0x00000001ffffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000000a00000-0x00000001ffffffff]
[    0.000000] On node 0, zone DMA32: 2560 pages in unavailable ranges
[    0.000000] SBI HSM extension detected
[    0.000000] Falling back to deprecated "riscv,isa"
[    0.000000] riscv: base ISA extensions acdfim
[    0.000000] riscv: ELF capabilities acdfim
[    0.000000] percpu: cpu 0 has no node -1 or node-local memory
[    0.000000] percpu: Embedded 31 pages/cpu s87528 r8192 d31256 u126976
[    0.000000] pcpu-alloc: s87528 r8192 d31256 u126976 alloc=31*4096
[    0.000000] pcpu-alloc: [0] 0 [0] 1 [0] 2 [0] 3 
[    0.000000] Kernel command line: BOOT_IMAGE=/vmlinuz-6.6.0-38.0.0.46.oe2403.riscv64 root=UUID=272f359e-3c38-4566-94b8-74bfd97cff39 console=ttyS0,115200 rootwait rw earlycon clk_ignore_unused loglevel=7 eth= rootrwoptions=rw,noatime rootrwreset=yes selinux=0
[    0.000000] Unknown kernel command line parameters "BOOT_IMAGE=/vmlinuz-6.6.0-38.0.0.46.oe2403.riscv64 eth= rootrwoptions=rw,noatime rootrwreset=yes", will be passed to user space.
[    0.000000] Dentry cache hash table entries: 1048576 (order: 11, 8388608 bytes, linear)
[    0.000000] Inode-cache hash table entries: 524288 (order: 10, 4194304 bytes, linear)
[    0.000000] Fallback order for Node 0: 0 
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 2061864
[    0.000000] Policy zone: Normal
[    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.000000] stackdepot: allocating hash table via alloc_large_system_hash
[    0.000000] stackdepot hash table entries: 524288 (order: 10, 4194304 bytes, linear)
[    0.000000] software IO TLB: area num 4.
[    0.000000] software IO TLB: mapped [mem 0x00000000f8000000-0x00000000fc000000] (64MB)
[    0.000000] Virtual kernel memory layout:
[    0.000000]       fixmap : 0xffffffc6fea00000 - 0xffffffc6ff000000   (6144 kB)
[    0.000000]       pci io : 0xffffffc6ff000000 - 0xffffffc700000000   (  16 MB)
[    0.000000]      vmemmap : 0xffffffc700000000 - 0xffffffc800000000   (4096 MB)
[    0.000000]      vmalloc : 0xffffffc800000000 - 0xffffffd800000000   (  64 GB)
[    0.000000]      modules : 0xffffffff024df000 - 0xffffffff80000000   (2011 MB)
[    0.000000]       lowmem : 0xffffffd800000000 - 0xffffffd9ff600000   (8182 MB)
[    0.000000]       kernel : 0xffffffff80000000 - 0xffffffffffffffff   (2047 MB)
[    0.000000] Memory: 7910212K/8378368K available (13933K kernel code, 5630K rwdata, 12955K rodata, 695K init, 4530K bss, 402620K reserved, 65536K cma-reserved)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=4, Nodes=1
[    0.000000] trace event string verifier disabled
[    0.000000] rcu: Preemptible hierarchical RCU implementation.
[    0.000000] rcu: 	RCU restricting CPUs from NR_CPUS=512 to nr_cpu_ids=4.
[    0.000000] 	Trampoline variant of Tasks RCU enabled.
[    0.000000] 	Tracing variant of Tasks RCU enabled.
[    0.000000] rcu: RCU calculated value of scheduler-enlistment delay is 25 jiffies.
[    0.000000] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
[    0.000000] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
[    0.000000] riscv-intc: 64 local interrupts mapped
[    0.000000] plic: interrupt-controller@ffd8000000: mapped 240 interrupts with 4 handlers for 8 contexts.
[    0.000000] riscv: providing IPIs using SBI IPI extension
[    0.000000] rcu: srcu_init: Setting srcu_struct sizes based on contention.
[    0.000000] clocksource: riscv_clocksource: mask: 0xffffffffffffffff max_cycles: 0x1623fa770, max_idle_ns: 881590404476 ns
[    0.000001] sched_clock: 64 bits at 3000kHz, resolution 333ns, wraps every 4398046511097ns
[    0.010764] Console: colour dummy device 80x25
[    0.017444] Calibrating delay loop (skipped), value calculated using timer frequency.. 6.00 BogoMIPS (lpj=12000)
[    0.028507] pid_max: default: 32768 minimum: 301
[    0.039821] LSM: initializing lsm=lockdown,capability,yama,apparmor,integrity
[    0.050063] Yama: becoming mindful.
[    0.054782] AppArmor: AppArmor initialized
[    0.061961] Mount-cache hash table entries: 16384 (order: 5, 131072 bytes, linear)
[    0.070423] Mountpoint-cache hash table entries: 16384 (order: 5, 131072 bytes, linear)
[    0.091374] RCU Tasks: Setting shift to 2 and lim to 1 rcu_task_cb_adjust=1.
[    0.099602] RCU Tasks Trace: Setting shift to 2 and lim to 1 rcu_task_cb_adjust=1.
[    0.108358] riscv: ELF compat mode unsupported
[    0.108477] ASID allocator using 16 bits (65536 entries)
[    0.120310] rcu: Hierarchical SRCU implementation.
[    0.125943] rcu: 	Max phase no-delay instances is 1000.
[    0.136598] Remapping and enabling EFI services.
[    0.143529] smp: Bringing up secondary CPUs ...
[    0.173000] cpu1: Ratio of byte access time to unaligned word access is 6.14, unaligned accesses are fast
[    0.209166] cpu2: Ratio of byte access time to unaligned word access is 6.14, unaligned accesses are fast
[    0.245345] cpu3: Ratio of byte access time to unaligned word access is 6.14, unaligned accesses are fast
[    0.256057] smp: Brought up 1 node, 4 CPUs
[    0.266681] devtmpfs: initialized
[    0.343981] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 7645041785100000 ns
[    0.354671] futex hash table entries: 1024 (order: 4, 65536 bytes, linear)
[    0.369188] pinctrl core: initialized pinctrl subsystem
[    0.383621] NET: Registered PF_NETLINK/PF_ROUTE protocol family
[    0.395754] DMA: preallocated 1024 KiB GFP_KERNEL pool for atomic allocations
[    0.405921] DMA: preallocated 1024 KiB GFP_KERNEL|GFP_DMA32 pool for atomic allocations
[    0.415022] audit: initializing netlink subsys (disabled)
[    0.421976] audit: type=2000 audit(0.336:1): state=initialized audit_enabled=0 res=1
[    0.424718] thermal_sys: Registered thermal governor 'fair_share'
[    0.430609] thermal_sys: Registered thermal governor 'step_wise'
[    0.437506] thermal_sys: Registered thermal governor 'user_space'
[    0.444309] thermal_sys: Registered thermal governor 'power_allocator'
[    0.451351] cpuidle: using governor menu
[    0.487502] cpu0: Ratio of byte access time to unaligned word access is 6.14, unaligned accesses are fast
[    0.512918] platform soc: Fixed dependency cycle(s) with /soc/interrupt-controller@ffd8000000
[    0.543340] platform dpu-encoders:dpu-encoder@0: Fixed dependency cycle(s) with /soc/dw-mipi-dsi0@ffef500000/dsi0-host
[    0.555273] platform ffef500000.dw-mipi-dsi0:dsi0-host: Fixed dependency cycle(s) with /soc/dw-mipi-dsi0@ffef500000/dsi0-host/panel0@0
[    0.568339] platform ffef500000.dw-mipi-dsi0:dsi0-host: Fixed dependency cycle(s) with /dpu-encoders/dpu-encoder@0
[    0.580801] platform ffef540000.dw-hdmi-tx: Fixed dependency cycle(s) with /soc/dc8200@ffef600000
[    0.591156] platform ffef540000.dw-hdmi-tx: Fixed dependency cycle(s) with /soc/dc8200@ffef600000
[    0.601264] platform dpu-encoders:dpu-encoder@0: Fixed dependency cycle(s) with /soc/dc8200@ffef600000
[    0.611746] platform ffef600000.dc8200: Fixed dependency cycle(s) with /soc/dw-hdmi-tx@ffef540000
[    0.621819] platform ffef600000.dc8200: Fixed dependency cycle(s) with /dpu-encoders/dpu-encoder@0
[    0.650411] platform fffc800000.vha: Fixed dependency cycle(s) with /soc/vha@fffc800000
[    0.677039] HugeTLB: registered 1.00 GiB page size, pre-allocated 0 pages
[    0.684786] HugeTLB: 16380 KiB vmemmap can be freed for a 1.00 GiB page
[    0.692238] HugeTLB: registered 2.00 MiB page size, pre-allocated 0 pages
[    0.699836] HugeTLB: 28 KiB vmemmap can be freed for a 2.00 MiB page
[    0.721146] ACPI: Interpreter disabled.
[    0.730289] iommu: Default domain type: Translated
[    0.735990] iommu: DMA domain TLB invalidation policy: lazy mode
[    0.746932] SCSI subsystem initialized
[    0.752401] libata version 3.00 loaded.
[    0.753346] usbcore: registered new interface driver usbfs
[    0.759840] usbcore: registered new interface driver hub
[    0.766180] usbcore: registered new device driver usb
[    0.773280] pps_core: LinuxPPS API ver. 1 registered
[    0.779055] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.790587] EDAC MC: Ver: 3.0.0
[    0.796713] efivars: Registered efivars operations
[    0.804501] Advanced Linux Sound Architecture Driver Initialized.
[    0.814621] NetLabel: Initializing
[    0.818855] NetLabel:  domain hash size = 128
[    0.823990] NetLabel:  protocols = UNLABELED CIPSOv4 CALIPSO
[    0.830737] NetLabel:  unlabeled traffic allowed by default
[    0.837761] vgaarb: loaded
[    0.843039] clocksource: Switched to clocksource riscv_clocksource
[    1.785041] VFS: Disk quotas dquot_6.6.0
[    1.790339] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    1.801857] AppArmor: AppArmor Filesystem Enabled
[    1.807642] pnp: PnP ACPI: disabled
[    1.852456] NET: Registered PF_INET protocol family
[    1.858802] IP idents hash table entries: 131072 (order: 8, 1048576 bytes, linear)
[    1.886062] tcp_listen_portaddr_hash hash table entries: 4096 (order: 4, 65536 bytes, linear)
[    1.895642] Table-perturb hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    1.904387] TCP established hash table entries: 65536 (order: 7, 524288 bytes, linear)
[    1.913499] TCP bind hash table entries: 65536 (order: 9, 2097152 bytes, linear)
[    1.924514] TCP: Hash tables configured (established 65536 bind 65536)
[    1.932269] UDP hash table entries: 4096 (order: 5, 131072 bytes, linear)
[    1.940052] UDP-Lite hash table entries: 4096 (order: 5, 131072 bytes, linear)
[    1.949108] NET: Registered PF_UNIX/PF_LOCAL protocol family
[    1.958786] RPC: Registered named UNIX socket transport module.
[    1.965659] RPC: Registered udp transport module.
[    1.971194] RPC: Registered tcp transport module.
[    1.976715] RPC: Registered tcp-with-tls transport module.
[    1.983009] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    1.990313] PCI: CLS 0 bytes, default 64
[    1.995756] Trying to unpack rootfs image as initramfs...
[    1.995790] kvm [1]: hypervisor extension not available
[    2.013920] Initialise system trusted keyrings
[    2.019353] Key type blacklist registered
[    2.024936] workingset: timestamp_bits=39 max_order=21 bucket_order=0
[    2.032347] zbud: loaded
[    2.057105] NFS: Registering the id_resolver key type
[    2.063229] Key type id_resolver registered
[    2.068241] Key type id_legacy registered
[    2.073378] nfs4filelayout_init: NFSv4 File Layout Driver Registering...
[    2.081048] jffs2: version 2.2. (NAND) © 2001-2006 Red Hat, Inc.
[    2.089508] fuse: init (API version 7.39)
[    2.096217] 9p: Installing v9fs 9p2000 file system support
[    2.104900] integrity: Platform Keyring initialized
[    2.179467] NET: Registered PF_ALG protocol family
[    2.185170] Key type asymmetric registered
[    2.190093] Asymmetric key parser 'x509' registered
[    2.196525] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 244)
[    2.205478] io scheduler mq-deadline registered
[    2.210817] io scheduler kyber registered
[    2.216123] io scheduler bfq registered
[    2.276642] shpchp: Standard Hot Plug PCI Controller Driver version: 0.4
[    2.293774] th1520_clocks_probe !!! start !
[    2.324424] th1520-fm-clk ffef010000.clock-controller: succeed to register th1520 fullmask clock driver
[    2.346333] visys-clk-gate-provider soc:visys-clk-gate: succeed to register visys gate clock provider
[    2.362112] vpsys-clk-gate-provider ffecc30000.vpsys-clk-gate: succeed to register vpsys gate clock provider
[    2.384976] vosys-clk-gate-provider ffef528000.vosys-clk-gate: succeed to register vosys gate clock provider
[    2.396943] dspsys-clk-gate-provider soc:dspsys-clk-gate: cannot find regmap for tee dsp system register
[    2.414579] dspsys-clk-gate-provider soc:dspsys-clk-gate: succeed to register dspsys gate clock provider
[    2.426180] th1520_audiosys_clk_probe audiosys_regmap=0xffffffd8ffb41c00
[    2.444287] audiosys-clk-gate-provider soc:audiosys-clk-gate: succeed to register audiosys gate clock provider
[    2.465942] miscsys-clk-gate-provider soc:miscsys-clk-gate: succeed to register miscsys gate clock provider
[    2.479550] dw_axi_dmac_platform ffefc00000.dma-controller: DesignWare AXI DMA Controller, 4 channels
[    2.495974] dw_axi_dmac_platform ffc8000000.dma-controller: DesignWare AXI DMA Controller, 16 channels
[    2.515708] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    2.530946] printk: console [ttyS0] disabled
[    2.537611] ffe7014000.serial: ttyS0 at MMIO 0xffe7014000 (irq = 20, base_baud = 6250000) is a 16550A
[    2.547947] printk: console [ttyS0] enabled
[    2.557945] printk: bootconsole [uart0] disabled
[    2.571622] SuperH (H)SCI(F) driver initialized
[    2.588007] vs-dc ffef600000.dc8200: dpu0pll_on:1 dpu1pll_on:1
[    2.604817] loop: module loaded
[    2.715340] rdac: device handler registered
[    2.720982] hp_sw: device handler registered
[    2.726047] emc: device handler registered
[    2.731805] alua: device handler registered
[    2.743453] SPI driver spidev has no spi_device_id for rohm,bh2228fv
[    2.753152] spi_norflash@0 enforce active low on GPIO handle
[    2.762487] spi-nor spi0.0: unrecognized JEDEC id bytes: ff ff ff ff ff ff
[    2.772110] spidev@0 enforce active low on GPIO handle
[    2.787593] e1000e: Intel(R) PRO/1000 Network Driver
[    2.793315] e1000e: Copyright(c) 1999 - 2015 Intel Corporation.
[    2.803195] th1520-dwmac ffe7070000.ethernet: IRQ eth_wake_irq not found
[    2.810674] th1520-dwmac ffe7070000.ethernet: IRQ eth_lpi not found
[    2.818130] th1520-dwmac ffe7070000.ethernet: PTP uses main clock
[    2.826501] th1520-dwmac ffe7070000.ethernet: User ID: 0x10, Synopsys ID: 0x37
[    2.834524] th1520-dwmac ffe7070000.ethernet: 	DWMAC1000
[    2.840583] th1520-dwmac ffe7070000.ethernet: DMA HW capability register supported
[    2.849004] th1520-dwmac ffe7070000.ethernet: RX Checksum Offload Engine supported
[    2.857320] th1520-dwmac ffe7070000.ethernet: COE Type 2
[    2.863372] th1520-dwmac ffe7070000.ethernet: TX Checksum insertion supported
[    2.871246] th1520-dwmac ffe7070000.ethernet: Enhanced/Alternate descriptors
[    2.879040] th1520-dwmac ffe7070000.ethernet: Enabled extended descriptors
[    2.886702] th1520-dwmac ffe7070000.ethernet: Ring mode enabled
[    2.893359] th1520-dwmac ffe7070000.ethernet: Enable RX Mitigation via HW Watchdog Timer
[    2.902188] th1520-dwmac ffe7070000.ethernet: device MAC address 56:0e:59:a5:6b:7a
[    3.709153] Freeing initrd memory: 25164K
[    3.786468] th1520-dwmac ffe7060000.ethernet: IRQ eth_wake_irq not found
[    3.794019] th1520-dwmac ffe7060000.ethernet: IRQ eth_lpi not found
[    3.801441] th1520-dwmac ffe7060000.ethernet: PTP uses main clock
[    3.809520] th1520-dwmac ffe7060000.ethernet: User ID: 0x10, Synopsys ID: 0x37
[    3.817579] th1520-dwmac ffe7060000.ethernet: 	DWMAC1000
[    3.823663] th1520-dwmac ffe7060000.ethernet: DMA HW capability register supported
[    3.831999] th1520-dwmac ffe7060000.ethernet: RX Checksum Offload Engine supported
[    3.840326] th1520-dwmac ffe7060000.ethernet: COE Type 2
[    3.846375] th1520-dwmac ffe7060000.ethernet: TX Checksum insertion supported
[    3.854268] th1520-dwmac ffe7060000.ethernet: Enhanced/Alternate descriptors
[    3.862069] th1520-dwmac ffe7060000.ethernet: Enabled extended descriptors
[    3.869690] th1520-dwmac ffe7060000.ethernet: Ring mode enabled
[    3.876360] th1520-dwmac ffe7060000.ethernet: Enable RX Mitigation via HW Watchdog Timer
[    3.885208] th1520-dwmac ffe7060000.ethernet: device MAC address 56:0e:59:a5:6b:7a
[    3.906447] usbcore: registered new interface driver uas
[    3.912712] usbcore: registered new interface driver usb-storage
[    3.919600] usbcore: registered new interface driver ums-sddr09
[    3.926382] usbcore: registered new interface driver ums-sddr55
[    3.933839] mousedev: PS/2 mouse device common for all mice
[    3.952501] rtc-efi rtc-efi.0: registered as rtc0
[    3.958261] rtc-efi rtc-efi.0: setting system clock to 2024-08-01T07:02:59 UTC (1722495779)
[    3.968597] i2c_dev: i2c /dev entries driver
[    3.977283] dw_wdt ffefc30000.watchdog: No valid TOPs array specified
[    3.986849] dw_wdt ffefc31000.watchdog: No valid TOPs array specified
[    3.997048] Watchdog module: th1520-wdt loaded
[    4.002305] device-mapper: core: CONFIG_IMA_DISABLE_HTABLE is disabled. Duplicate IMA measurements will not be recorded in the IMA log.
[    4.016411] device-mapper: ioctl: 4.48.0-ioctl (2023-03-01) initialised: dm-devel@redhat.com
[    4.027616] sdhci: Secure Digital Host Controller Interface driver
[    4.034581] sdhci: Copyright(c) Pierre Ossman
[    4.040572] sdhci-pltfm: SDHCI platform and OF driver helper
[    4.051215] hid: raw HID events driver (C) Jiri Kosina
[    4.060937] usbcore: registered new interface driver usbhid
[    4.067521] usbhid: USB HID core driver
[    4.075627] xuantie,th1520-mbox-client mbox_910t_client2: Successfully registered
[    4.085202] riscv-pmu-sbi: SBI PMU extension is available
[    4.091094] mmc0: SDHCI controller on ffe7080000.mmc [ffe7080000.mmc] using ADMA 64-bit
[    4.091683] riscv-pmu-sbi: 16 firmware and 18 hardware counters
[    4.103143] mmc2: SDHCI controller on ffe7090000.mmc [ffe7090000.mmc] using ADMA 64-bit
[    4.146804] mmc0: new HS400 MMC card at address 0001
[    4.149234] mmc2: new high speed SDXC card at address b368
[    4.156530] mmcblk0: mmc0:0001 Y2P032 29.1 GiB
[    4.162491] mmcblk2: mmc2:b368 NCard 58.2 GiB
[    4.191251] mmc1: SDHCI controller on ffe70a0000.mmc [ffe70a0000.mmc] using ADMA 64-bit
[    4.201095]  mmcblk0: p1 p2 p3 p4 p5
[    4.205806] sdhci-dwcmshc ffe70a0000.mmc: card claims to support voltages below defined range
[    4.212256] mmcblk0boot0: mmc0:0001 Y2P032 4.00 MiB
[    4.217832] GPT:Primary header thinks Alt. header is not at the end of the disk.
[    4.224838] NET: Registered PF_INET6 protocol family
[    4.229057] mmcblk0boot1: mmc0:0001 Y2P032 4.00 MiB
[    4.229069] GPT:33554431 != 122138623
[    4.229082] GPT:Alternate GPT header not at the end of the disk.
[    4.229087] GPT:33554431 != 122138623
[    4.229095] GPT: Use GNU Parted to correct GPT errors.
[    4.229167]  mmcblk2: p1 p2 p3
[    4.240807] Segment Routing with IPv6
[    4.242029] mmc1: new high speed SDIO card at address 0001
[    4.251394] mmcblk0rpmb: mmc0:0001 Y2P032 16.0 MiB, chardev (238:0)
[    4.251909] In-situ OAM (IOAM) with IPv6
[    4.288279] NET: Registered PF_PACKET protocol family
[    4.294219] bridge: filtering via arp/ip/ip6tables is no longer available by default. Update your scripts to load br_netfilter if you need this.
[    4.309097] 9pnet: Installing 9P2000 support
[    4.314441] Key type dns_resolver registered
[    4.396274] registered taskstats version 1
[    4.405774] Loading compiled-in X.509 certificates
[    4.520153] Loaded X.509 cert 'Build time autogenerated kernel key: 0fe359f391fd1720705b062b28acceca43ebaf4d'
[    4.589288] kmemleak: Kernel memory leak detector initialized (mem pool available: 15664)
[    4.589300] kmemleak: Automatic memory scanning thread started
[    4.645270] Key type encrypted registered
[    4.650095] AppArmor: AppArmor sha1 policy hashing enabled
[    4.656794] ima: No TPM chip found, activating TPM-bypass!
[    4.663191] ima: Allocated hash algorithm: sha256
[    4.668837] ima: No architecture policies found
[    4.674331] evm: Initialising EVM extended attributes:
[    4.680232] evm: security.selinux
[    4.684300] evm: security.SMACK64 (disabled)
[    4.689298] evm: security.SMACK64EXEC (disabled)
[    4.694666] evm: security.SMACK64TRANSMUTE (disabled)
[    4.700467] evm: security.SMACK64MMAP (disabled)
[    4.705811] evm: security.apparmor
[    4.709954] evm: security.ima
[    4.713644] evm: security.capability
[    4.717958] evm: HMAC attrs: 0x1
[    5.912545] es7210 work-mode not defined.using ES7210_NORMAL_I2S by default
[    5.920316] es7210 channels-max not defined.using MIC_CHN_2 by default
[    5.934279] pca953x 0-0018: supply vcc not found, using dummy regulator
[    5.942374] pca953x 0-0018: using no AI
[    5.952773] pca953x 1-0018: supply vcc not found, using dummy regulator
[    5.960755] pca953x 1-0018: using no AI
[    5.972865] pca953x 3-0018: supply vcc not found, using dummy regulator
[    5.981083] pca953x 3-0018: using no AI
[    5.991634] dw-mipi-dsi ffef500000.dw-mipi-dsi0:dsi0-host: Fixed dependency cycle(s) with /soc/dw-mipi-dsi0@ffef500000/dsi0-host/panel0@0
[    6.005360] mipi-dsi ffef500000.dw-mipi-dsi0:dsi0-host.0: Fixed dependency cycle(s) with /soc/dw-mipi-dsi0@ffef500000/dsi0-host
[    6.022945] vs-drm display-subsystem: bound ffef600000.dc8200 (ops dc_component_ops)
[    6.036013] virtual_log_mem=0x(____ptrval____), phy base=0x0x0000000033600000
[    6.165744] th1520-event soc:th1520-event: magicnum:0x869851a9 mode:0xe0edc299
[    6.173770] th1520-event soc:th1520-event: set rebootmode:0x2
[    6.180268] th1520-event soc:th1520-event: th1520-event driver init successfully
[    6.196073] vs-drm display-subsystem: bound ffef600000.dc8200 (ops dc_component_ops)
[    6.204742] vs-drm display-subsystem: bound ffef500000.dw-mipi-dsi0:dsi0-host (ops dsi_component_ops)
[    6.215554] dwhdmi-th1520 ffef540000.dw-hdmi-tx: Detected HDMI TX controller v2.14a with HDCP (DWC HDMI 2.0 TX PHY)
[    6.229222] dwhdmi-th1520 ffef540000.dw-hdmi-tx: registered DesignWare HDMI I2C bus driver
[    6.239434] vs-drm display-subsystem: bound ffef540000.dw-hdmi-tx (ops dw_hdmi_th1520_ops)
[    6.248651] vs-drm display-subsystem: bound dpu-encoders:dpu-encoder@0 (ops encoder_component_ops)
[    6.262153] [drm] Initialized vs-drm 1.0.0 20191101 for display-subsystem on minor 0
[    6.279247] fbcon: Deferring console take-over
[    6.284576] vs-drm display-subsystem: [drm] fb0: vs-drmdrmfb frame buffer device
[    6.293720] dw_dphy_phy_write: test_code = 0xa3, len = 1, data[0] = 0x0
[    6.294926] [th1520_wdt_probe,346] register power off callback
[    6.301171] dw_dphy_phy_write: test_code = 0xa0, len = 1, data[0] = 0x1
[    6.307751] succeed to register th1520 pmic watchdog
[    6.315083] dw_dphy_phy_write: test_code = 0x4a, len = 1, data[0] = 0x40
[    6.328087] cpu cpu0: EM: OPP:1704000 is inefficient
[    6.328186] cpu cpu0: EM: OPP:1608000 is inefficient
[    6.328201] cpu cpu0: EM: OPP:1404000 is inefficient
[    6.328220] cpu cpu0: EM: OPP:1296000 is inefficient
[    6.328232] cpu cpu0: EM: OPP:1200000 is inefficient
[    6.328231] dw_dphy_phy_write: test_code = 0xe, len = 1, data[0] = 0x8
[    6.328244] cpu cpu0: EM: OPP:1104000 is inefficient
[    6.328253] cpu cpu0: EM: OPP:1000000 is inefficient
[    6.335512] dw_dphy_phy_write: test_code = 0x1c, len = 1, data[0] = 0x10
[    6.335521] cpu cpu0: EM: OPP:900000 is inefficient
[    6.335531] cpu cpu0: EM: OPP:702000 is inefficient
[    6.342956] cpu cpu0: EM: OPP:600000 is inefficient
[    6.342967] cpu cpu0: EM: OPP:500000 is inefficient
[    6.342978] cpu cpu0: EM: OPP:400000 is inefficient
[    6.342992] cpu cpu0: EM: Access to CPUFreq policy failed
[    6.351591] cpu cpu0: EM: created perf domain
[    6.358276] cpufreq: cpufreq_online: CPU0: Running at unlisted initial frequency: 750000 KHz, changing to: 800000 KHz
[    6.372236] cpu cpu0: finish to register cpufreq driver
[    6.380098] th1520 rpmsg: Ready for cross core communication!
[    6.386658] th1520 rpmsg: rproc_name = m4
[    6.389217] virtio_rpmsg_bus virtio0: rpmsg host is online
[    6.400316] virtual_log_mem=0x(____ptrval____), phy base=0x0x0000000033400000
[    6.405288] virtio_rpmsg_bus virtio0: msg received with no recipient
[    6.408397] th1520 rpmsg: driver is registered.
[    6.427590] clk: Not disabling unused clocks
[    6.432670] ALSA device list:
[    6.436375]   #0: TH1520-Sound-Card
[    6.440950] dw-apb-uart ffe7014000.serial: forbid DMA for kernel console
[    6.448499] integrity: Unable to open file: /etc/keys/x509_ima.der (-2)
[    6.448551] integrity: Unable to open file: /etc/keys/x509_evm.der (-2)
[    6.456091] Freeing unused kernel image (initmem) memory: 692K
[    6.469968] Kernel memory protection not selected by kernel config.
[    6.476980] Run /init as init process
[    6.481353]   with arguments:
[    6.481358]     /init
[    6.481362]   with environment:
[    6.481366]     HOME=/
[    6.481370]     TERM=linux
[    6.481374]     BOOT_IMAGE=/vmlinuz-6.6.0-38.0.0.46.oe2403.riscv64
[    6.481377]     eth=
[    6.481382]     rootrwoptions=rw,noatime
[    6.481387]     rootrwreset=yes
[    6.548320] systemd[1]: systemd v255-18.oe2403 running in system mode (+PAM +AUDIT +SELINUX -APPARMOR +IMA -SMACK +SECCOMP +GCRYPT +GNUTLS -OPENSSL +ACL +BLKID -CURL -ELFUTILS -FIDO2 +IDN2 -IDN -IPTC +KMOD +LIBCRYPTSETUP +LIBFDISK +PCRE2 -PWQUALITY +P11KIT -QRENCODE -TPM2 +BZIP2 +LZ4 +XZ +ZLIB -ZSTD -BPF_FRAMEWORK +XKBCOMMON +UTMP +SYSVINIT default-hierarchy=legacy)
[    6.581982] systemd[1]: Detected architecture riscv64.
[    6.588076] systemd[1]: Running in initrd.
[    6.663945] systemd[1]: Hostname set to <openeuler-riscv64>.
[    7.056248] systemd[1]: Queued start job for default target Initrd Default Target.
[    7.165121] systemd[1]: Reached target Initrd /usr File System.
[    7.187334] systemd[1]: Reached target Slice Units.
[    7.207269] systemd[1]: Reached target Swaps.
[    7.227231] systemd[1]: Reached target Timer Units.
[    7.248068] systemd[1]: Listening on Journal Socket (/dev/log).
[    7.272122] systemd[1]: Listening on Journal Socket.
[    7.292290] systemd[1]: Listening on udev Control Socket.
[    7.315920] systemd[1]: Listening on udev Kernel Socket.
[    7.339260] systemd[1]: Reached target Socket Units.
[    7.407787] systemd[1]: Starting Create List of Static Device Nodes...
[    7.437019] systemd[1]: Started Hardware RNG Entropy Gatherer Daemon.
[    7.464244] systemd[1]: systemd-journald.service: unit configures an IP firewall, but the local system does not support BPF/cgroup firewalling.
[    7.477960] systemd[1]: systemd-journald.service: (This warning is only shown for the first unit using IP firewalling.)
[    7.513709] systemd[1]: Starting Journal Service...
[    7.545050] systemd[1]: Starting Load Kernel Modules...
[    7.576435] systemd[1]: Starting Create Static Device Nodes in /dev...
[    7.612599] systemd-journald[254]: Collecting audit messages is disabled.
[    7.636100] systemd[1]: Starting Virtual Console Setup...
[    7.664356] systemd[1]: Finished Create List of Static Device Nodes.
[    7.697564] systemd[1]: Finished Load Kernel Modules.
[    7.730358] systemd[1]: Finished Create Static Device Nodes in /dev.
[    7.745004] systemd[1]: Reached target Preparation for Local File Systems.
[    7.763442] systemd[1]: Reached target Local File Systems.
[    7.812095] systemd[1]: Starting Apply Kernel Variables...
[    7.827709] systemd[1]: Started Journal Service.
[    9.063113] random: crng init done
[   11.768596] th1520-dwmac ffe7070000.ethernet end0: renamed from eth0
[   11.870969] th1520-dwmac ffe7060000.ethernet end1: renamed from eth1
[   14.058066] EXT4-fs (mmcblk2p3): mounted filesystem 272f359e-3c38-4566-94b8-74bfd97cff39 r/w with ordered data mode. Quota mode: none.
[   15.070149] systemd-journald[254]: Received SIGTERM from PID 1 (systemd).
[   15.943844] systemd[1]: systemd v255-18.oe2403 running in system mode (+PAM +AUDIT +SELINUX -APPARMOR +IMA -SMACK +SECCOMP +GCRYPT +GNUTLS -OPENSSL +ACL +BLKID -CURL -ELFUTILS -FIDO2 +IDN2 -IDN -IPTC +KMOD +LIBCRYPTSETUP +LIBFDISK +PCRE2 -PWQUALITY +P11KIT -QRENCODE -TPM2 +BZIP2 +LZ4 +XZ +ZLIB -ZSTD -BPF_FRAMEWORK +XKBCOMMON +UTMP +SYSVINIT default-hierarchy=legacy)
[   15.977408] systemd[1]: Detected architecture riscv64.
[   16.291685] systemd-sysv-generator[534]: SysV service '/etc/rc.d/init.d/S02bt' lacks a native systemd unit file. ♻️ Automatically generating a unit file for compatibility. Please update package to include a native systemd unit file, in order to make it safe, robust and future-proof. ⚠️ This compatibility logic is deprecated, expect removal soon. ⚠️
[   16.811275] systemd[1]: /usr/lib/systemd/system/dbus.socket:5: ListenStream= references a path below legacy directory /var/run/, updating /var/run/dbus/system_bus_socket → /run/dbus/system_bus_socket; please update the unit file accordingly.
[   17.413767] systemd[1]: /usr/lib/systemd/system/alsa-restore.service:15: Standard output type syslog is obsolete, automatically updating to journal. Please update your unit file, and consider removing the setting altogether.
[   17.664892] systemd[1]: initrd-switch-root.service: Deactivated successfully.
[   17.675352] systemd[1]: Stopped Switch Root.
[   17.685581] systemd[1]: systemd-journald.service: Scheduled restart job, restart counter is at 1.
[   17.699645] systemd[1]: Created slice Slice /system/getty.
[   17.710567] systemd[1]: Created slice Slice /system/serial-getty.
[   17.722250] systemd[1]: Created slice Slice /system/sshd-keygen.
[   17.732455] systemd[1]: Created slice User and Session Slice.
[   17.740671] systemd[1]: Dispatch Password Requests to Console Directory Watch was skipped because of an unmet condition check (ConditionPathExists=!/run/plymouth/pid).
[   17.757354] systemd[1]: Started Forward Password Requests to Wall Directory Watch.
[   17.767809] systemd[1]: Set up automount Arbitrary Executable File Formats File System Automount Point.
[   17.779686] systemd[1]: Stopped target Switch Root.
[   17.786417] systemd[1]: Stopped target Initrd File Systems.
[   17.794039] systemd[1]: Stopped target Initrd Root File System.
[   17.802149] systemd[1]: Reached target User and Group Name Lookups.
[   17.810393] systemd[1]: Reached target Path Units.
[   17.817313] systemd[1]: Reached target Slice Units.
[   17.824899] systemd[1]: Listening on Device-mapper event daemon FIFOs.
[   17.835452] systemd[1]: Listening on LVM2 poll daemon socket.
[   17.845553] systemd[1]: Listening on RPCbind Server Activation Socket.
[   17.853604] systemd[1]: Reached target RPC Port Mapper.
[   17.869275] systemd[1]: Listening on Process Core Dump Socket.
[   17.877516] systemd[1]: Listening on initctl Compatibility Named Pipe.
[   17.888246] systemd[1]: Listening on udev Control Socket.
[   17.896781] systemd[1]: Listening on udev Kernel Socket.
[   17.939660] systemd[1]: Mounting Huge Pages File System...
[   17.953289] systemd[1]: Mounting POSIX Message Queue File System...
[   17.968083] systemd[1]: Mounting Kernel Debug File System...
[   17.984558] systemd[1]: Mounting Kernel Trace File System...
[   17.992353] systemd[1]: Kernel Module supporting RPCSEC_GSS was skipped because of an unmet condition check (ConditionPathExists=/etc/krb5.keytab).
[   18.020216] systemd[1]: Starting Create List of Static Device Nodes...
[   18.044253] systemd[1]: Starting Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling...
[   18.063349] systemd[1]: Starting Load Kernel Module configfs...
[   18.078957] systemd[1]: Starting Load Kernel Module drm...
[   18.094811] systemd[1]: Starting Load Kernel Module fuse...
[   18.103125] systemd[1]: plymouth-switch-root.service: Deactivated successfully.
[   18.113291] systemd[1]: Stopped Plymouth switch root service.
[   18.122156] systemd[1]: systemd-fsck-root.service: Deactivated successfully.
[   18.133359] systemd[1]: Stopped File System Check on Root Device.
[   18.152677] systemd[1]: Starting Journal Service...
[   18.199944] systemd[1]: Starting Load Kernel Modules...
[   18.213612] systemd[1]: Starting Generate network units from Kernel command line...
[   18.232635] systemd[1]: Starting Remount Root and Kernel File Systems...
[   18.256106] systemd[1]: Starting Coldplug All udev Devices...
[   18.272993] systemd[1]: Mounted Huge Pages File System.
[   18.281281] systemd[1]: Mounted POSIX Message Queue File System.
[   18.291828] systemd[1]: Mounted Kernel Debug File System.
[   18.299827] systemd[1]: Mounted Kernel Trace File System.
[   18.301581] systemd-journald[555]: Collecting audit messages is disabled.
[   18.319772] systemd[1]: Finished Create List of Static Device Nodes.
[   18.329878] systemd[1]: modprobe@configfs.service: Deactivated successfully.
[   18.340764] systemd[1]: Finished Load Kernel Module configfs.
[   18.349750] systemd[1]: Started Journal Service.
[   18.644970] systemd-journald[555]: Received client request to flush runtime journal.
[   18.774443] systemd-journald[555]: File /var/log/journal/d7a3e7be6346430fbac058b7da6dda4c/system.journal corrupted or uncleanly shut down, renaming and replacing.
[   20.620007] Adding 1572860k swap on /dev/mmcblk0p3.  Priority:-2 extents:1 across:1572860k SS
[   20.843592] dwc3-xuantie ffec03f000.usb: th1520 dwc3 probe ok!
[   20.916097] xuantie-th1520-adc fffff51000.adc: XuanTie TH1520 adc registered.
[   20.931397] xgene-rtc fffff40000.rtc: registered as rtc1
[   21.594494] xhci-hcd xhci-hcd.3.auto: xHCI Host Controller
[   21.603152] xhci-hcd xhci-hcd.3.auto: new USB bus registered, assigned bus number 1
[   21.613246] xhci-hcd xhci-hcd.3.auto: hcc params 0x0220fe65 hci version 0x110 quirks 0x0000008000000810
[   21.623607] xhci-hcd xhci-hcd.3.auto: irq 42, io mem 0xffe7040000
[   21.624063] xhci-hcd xhci-hcd.3.auto: xHCI Host Controller
[   21.631358] xhci-hcd xhci-hcd.3.auto: new USB bus registered, assigned bus number 2
[   21.631396] xhci-hcd xhci-hcd.3.auto: Host supports USB 3.0 SuperSpeed
[   21.631878] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 6.06
[   21.667783] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   21.676797] usb usb1: Product: xHCI Host Controller
[   21.683719] usb usb1: Manufacturer: Linux 6.6.0-38.0.0.46.oe2403.riscv64 xhci-hcd
[   21.693298] usb usb1: SerialNumber: xhci-hcd.3.auto
[   21.701367] hub 1-0:1.0: USB hub found
[   21.706192] hub 1-0:1.0: 1 port detected
[   21.715999] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 6.06
[   21.725086] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   21.733082] usb usb2: Product: xHCI Host Controller
[   21.733092] usb usb2: Manufacturer: Linux 6.6.0-38.0.0.46.oe2403.riscv64 xhci-hcd
[   21.733097] usb usb2: SerialNumber: xhci-hcd.3.auto
[   21.734774] hub 2-0:1.0: USB hub found
[   21.762645] hub 2-0:1.0: 1 port detected
[   21.865360] usbcore: registered new device driver onboard-usb-hub
[   22.007097] usb 1-1: new high-speed USB device number 2 using xhci-hcd
[   22.040171] cfg80211: Loading compiled-in X.509 certificates for regulatory database
[   22.049475] Loaded X.509 cert 'sforshee: 00b28ddf47aef9cea7'
[   22.056797] Loaded X.509 cert 'wens: 61c038651aabdcf94bd0ac7ff06c7248db18c600'
[   22.174189] usb 1-1: New USB device found, idVendor=2109, idProduct=2817, bcdDevice= 7.00
[   22.183830] usb 1-1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[   22.192997] usb 1-1: Product: USB2.0 Hub             
[   22.198875] usb 1-1: Manufacturer: VIA Labs, Inc.         
[   22.243762] hub 1-1:1.0: USB hub found
[   22.263122] hub 1-1:1.0: 5 ports detected
[   22.311239] usb 2-1: new SuperSpeed USB device number 2 using xhci-hcd
[   22.509804] usb 2-1: New USB device found, idVendor=2109, idProduct=0817, bcdDevice= 7.00
[   22.523638] usb 2-1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[   22.531555] usb 2-1: Product: USB3.0 Hub             
[   22.537401] usb 2-1: Manufacturer: VIA Labs, Inc.         
[   22.611225] hub 2-1:1.0: USB hub found
[   22.628341] hub 2-1:1.0: 4 ports detected
[   22.996169] rtw_8723ds mmc1:0001:1: Firmware version 48.0.0, H2C version 0
[   23.071374] usb 1-1.5: new high-speed USB device number 3 using xhci-hcd
[   23.223777] usb 1-1.5: New USB device found, idVendor=2109, idProduct=8817, bcdDevice= 0.01
[   23.244280] usb 1-1.5: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[   23.269954] usb 1-1.5: Product: USB Billboard Device   
[   23.276434] usb 1-1.5: Manufacturer: VIA Labs, Inc.         
[   23.276459] usb 1-1.5: SerialNumber: 0000000000000001
[   32.748676] th1520-dwmac ffe7070000.ethernet end0: Register MEM_TYPE_PAGE_POOL RxQ-0
[   32.946917] th1520-dwmac ffe7070000.ethernet end0: PHY [stmmac-0:01] driver [RTL8211F Gigabit Ethernet] (irq=POLL)
[   32.966912] dwmac1000: Master AXI performs any burst length
[   32.973390] th1520-dwmac ffe7070000.ethernet end0: No Safety Features support found
[   32.994945] th1520-dwmac ffe7070000.ethernet end0: IEEE 1588-2008 Advanced Timestamp supported
[   33.019076] th1520-dwmac ffe7070000.ethernet end0: configuring for phy/rgmii-id link mode
[   33.143444] th1520-dwmac ffe7060000.ethernet end1: Register MEM_TYPE_PAGE_POOL RxQ-0
[   33.343203] th1520-dwmac ffe7060000.ethernet end1: PHY [stmmac-0:02] driver [RTL8211F Gigabit Ethernet] (irq=POLL)
[   33.362915] dwmac1000: Master AXI performs any burst length
[   33.369360] th1520-dwmac ffe7060000.ethernet end1: No Safety Features support found
[   33.390923] th1520-dwmac ffe7060000.ethernet end1: IEEE 1588-2008 Advanced Timestamp supported
[   33.411892] th1520-dwmac ffe7060000.ethernet end1: configuring for phy/rgmii-id link mode
[   35.535404] th1520-dwmac ffe7060000.ethernet end1: Link is Up - 100Mbps/Full - flow control rx/tx
[   38.054846] soc_dovdd18_scan: disabling
[   38.059624] soc_dvdd12_scan: disabling
[   38.064290] soc_avdd28_scan_en: disabling
