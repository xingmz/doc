[    0.000000][    T0] Linux version 6.6.0-44 (openeuler@openeuler-riscv64) (gcc (GCC) 12.3.1 (openEuler 12.3.1-16.oe2309), GNU ld (GNU Binutils) 2.40) #1 SMP Fri M4
[    0.000000][    T0] random: crng init done
[    0.000000][    T0] Machine model: Sipeed Lichee Pi 4A
[    0.000000][    T0] SBI specification v2.0 detected
[    0.000000][    T0] SBI implementation ID=0x1 Version=0x10004
[    0.000000][    T0] SBI TIME extension detected
[    0.000000][    T0] SBI IPI extension detected
[    0.000000][    T0] SBI RFENCE extension detected
[    0.000000][    T0] earlycon: uart0 at MMIO32 0x000000ffe7014000 (options '115200n8')
[    0.000000][    T0] printk: bootconsole [uart0] enabled
[    0.000000][    T0] efi: UEFI not found.
[    0.000000][    T0] OF: reserved mem: 0x0000000000000000..0x000000000003ffff (256 KiB) nomap non-reusable mmode_resv1@0
[    0.000000][    T0] OF: reserved mem: 0x0000000000040000..0x000000000005ffff (128 KiB) nomap non-reusable mmode_resv0@40000
[    0.000000][    T0] OF: reserved mem: 0x000000001e000000..0x000000001e00ffff (64 KiB) map non-reusable memory@1E000000
[    0.000000][    T0] cma: Reserved 64 MiB at 0x00000000fc000000 on node -1
[    0.000000][    T0] NUMA: No NUMA configuration found
[    0.000000][    T0] NUMA: Faking a node at [mem 0x0000000000000000-0x00000001ffffffff]
[    0.000000][    T0] NUMA: NODE_DATA [mem 0x1fffc6280-0x1fffcafff]
[    0.000000][    T0] Zone ranges:
[    0.000000][    T0]   DMA32    [mem 0x0000000000000000-0x00000000ffffffff]
[    0.000000][    T0]   Normal   [mem 0x0000000100000000-0x00000001ffffffff]
[    0.000000][    T0] Movable zone start for each node
[    0.000000][    T0] Early memory node ranges
[    0.000000][    T0]   node   0: [mem 0x0000000000000000-0x000000000005ffff]
[    0.000000][    T0]   node   0: [mem 0x0000000000060000-0x00000001ffffffff]
[    0.000000][    T0] Initmem setup node 0 [mem 0x0000000000000000-0x00000001ffffffff]
[    0.000000][    T0] SBI HSM extension detected
[    0.000000][    T0] Falling back to deprecated "riscv,isa"
[    0.000000][    T0] riscv: base ISA extensions acdfim
[    0.000000][    T0] riscv: ELF capabilities acdfim
[    0.000000][    T0] percpu: cpu 0 has no node -1 or node-local memory
[    0.000000][    T0] percpu: Embedded 30 pages/cpu s85928 r8192 d28760 u122880
[    0.000000][    T0] Kernel command line: root=/dev/mmcblk0p3 console=ttyS0,115200 rootwait rw earlycon clk_ignore_unused loglevel=7 eth= rootrwoptions=rw,noatimes
[    0.000000][    T0] Unknown kernel command line parameters "eth= rootrwoptions=rw,noatime rootrwreset=yes", will be passed to user space.
[    0.000000][    T0] Dentry cache hash table entries: 1048576 (order: 11, 8388608 bytes, linear)
[    0.000000][    T0] Inode-cache hash table entries: 524288 (order: 10, 4194304 bytes, linear)
[    0.000000][    T0] Fallback order for Node 0: 0 
[    0.000000][    T0] Built 1 zonelists, mobility grouping on.  Total pages: 2064384
[    0.000000][    T0] Policy zone: Normal
[    0.000000][    T0] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.000000][    T0] stackdepot: allocating hash table via alloc_large_system_hash
[    0.000000][    T0] stackdepot hash table entries: 524288 (order: 10, 4194304 bytes, linear)
[    0.000000][    T0] software IO TLB: area num 4.
[    0.000000][    T0] software IO TLB: mapped [mem 0x00000000f8000000-0x00000000fc000000] (64MB)
[    0.000000][    T0] Virtual kernel memory layout:
[    0.000000][    T0]       fixmap : 0xffffffc6fea00000 - 0xffffffc6ff000000   (6144 kB)
[    0.000000][    T0]       pci io : 0xffffffc6ff000000 - 0xffffffc700000000   (  16 MB)
[    0.000000][    T0]      vmemmap : 0xffffffc700000000 - 0xffffffc800000000   (4096 MB)
[    0.000000][    T0]      vmalloc : 0xffffffc800000000 - 0xffffffd800000000   (  64 GB)
[    0.000000][    T0]      modules : 0xffffffff029d7000 - 0xffffffff80000000   (2006 MB)
[    0.000000][    T0]       lowmem : 0xffffffd800000000 - 0xffffffda00000000   (8192 MB)
[    0.000000][    T0]       kernel : 0xffffffff80000000 - 0xffffffffffffffff   (2047 MB)
[    0.000000][    T0] Memory: 8048452K/8388608K available (13288K kernel code, 8962K rwdata, 12288K rodata, 2757K init, 4492K bss, 274620K reserved, 65536K cma-res)
[    0.000000][    T0] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=4, Nodes=1
[    0.000000][    T0] ftrace: allocating 50646 entries in 198 pages
[    0.000000][    T0] ftrace: allocated 198 pages with 4 groups
[    0.000000][    T0] trace event string verifier disabled
[    0.000000][    T0] rcu: Hierarchical RCU implementation.
[    0.000000][    T0] rcu:     RCU restricting CPUs from NR_CPUS=512 to nr_cpu_ids=4.
[    0.000000][    T0]  Rude variant of Tasks RCU enabled.
[    0.000000][    T0]  Tracing variant of Tasks RCU enabled.
[    0.000000][    T0] rcu: RCU calculated value of scheduler-enlistment delay is 25 jiffies.
[    0.000000][    T0] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
[    0.000000][    T0] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
[    0.000000][    T0] riscv-intc: 64 local interrupts mapped
[    0.000000][    T0] plic: interrupt-controller@ffd8000000: mapped 240 interrupts with 4 handlers for 8 contexts.
[    0.000000][    T0] riscv: providing IPIs using SBI IPI extension
[    0.000000][    T0] rcu: srcu_init: Setting srcu_struct sizes based on contention.
[    0.000000][    T0] clocksource: riscv_clocksource: mask: 0xffffffffffffffff max_cycles: 0x1623fa770, max_idle_ns: 881590404476 ns
[    0.000001][    T0] sched_clock: 64 bits at 3000kHz, resolution 333ns, wraps every 4398046511097ns
[    0.011111][    T0] Console: colour dummy device 80x25
[    0.016867][    T0] Calibrating delay loop (skipped), value calculated using timer frequency.. 6.00 BogoMIPS (lpj=12000)
[    0.028255][    T0] pid_max: default: 32768 minimum: 301
[    0.034691][    T0] LSM: initializing lsm=lockdown,capability,yama,selinux,integrity
[    0.043129][    T0] Yama: becoming mindful.
[    0.047564][    T0] SELinux:  Initializing.
[    0.053317][    T0] Mount-cache hash table entries: 16384 (order: 5, 131072 bytes, linear)
[    0.062046][    T0] Mountpoint-cache hash table entries: 16384 (order: 5, 131072 bytes, linear)
[    0.078520][    T1] RCU Tasks Rude: Setting shift to 2 and lim to 1 rcu_task_cb_adjust=1.
[    0.087436][    T1] RCU Tasks Trace: Setting shift to 2 and lim to 1 rcu_task_cb_adjust=1.
[    0.096387][    T1] riscv: ELF compat mode unsupported
[    0.096479][    T1] ASID allocator using 16 bits (65536 entries)
[    0.108582][    T1] rcu: Hierarchical SRCU implementation.
[    0.114372][    T1] rcu:     Max phase no-delay instances is 1000.
[    0.124140][    T1] EFI services will not be available.
[    0.130750][    T1] smp: Bringing up secondary CPUs ...
[    0.160379][    T0] cpu1: Ratio of byte access time to unaligned word access is 6.14, unaligned accesses are fast
[    0.196544][    T0] cpu2: Ratio of byte access time to unaligned word access is 6.14, unaligned accesses are fast
[    0.232693][    T0] cpu3: Ratio of byte access time to unaligned word access is 6.14, unaligned accesses are fast
[    0.243715][    T1] smp: Brought up 1 node, 4 CPUs
[    0.255992][    T1] devtmpfs: initialized
[    0.306595][    T1] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 7645041785100000 ns
[    0.317661][    T1] futex hash table entries: 1024 (order: 4, 65536 bytes, linear)
[    0.328939][    T1] pinctrl core: initialized pinctrl subsystem
[    0.341646][    T1] NET: Registered PF_NETLINK/PF_ROUTE protocol family
[    0.351554][    T1] DMA: preallocated 1024 KiB GFP_KERNEL pool for atomic allocations
[    0.360320][    T1] DMA: preallocated 1024 KiB GFP_KERNEL|GFP_DMA32 pool for atomic allocations
[    0.369700][    T1] audit: initializing netlink subsys (disabled)
[    0.376743][   T41] audit: type=2000 audit(0.260:1): state=initialized audit_enabled=0 res=1
[    0.379147][    T1] thermal_sys: Registered thermal governor 'fair_share'
[    0.385596][    T1] thermal_sys: Registered thermal governor 'step_wise'
[    0.392771][    T1] thermal_sys: Registered thermal governor 'user_space'
[    0.399933][    T1] cpuidle: using governor menu
[    0.436039][    T1] cpu0: Ratio of byte access time to unaligned word access is 6.14, unaligned accesses are fast
[    0.453335][    T1] platform soc: Fixed dependency cycle(s) with /soc/interrupt-controller@ffd8000000
[    0.489240][    T1] HugeTLB: registered 1.00 GiB page size, pre-allocated 0 pages
[    0.497178][    T1] HugeTLB: 16380 KiB vmemmap can be freed for a 1.00 GiB page
[    0.504841][    T1] HugeTLB: registered 2.00 MiB page size, pre-allocated 0 pages
[    0.512676][    T1] HugeTLB: 28 KiB vmemmap can be freed for a 2.00 MiB page
[    0.530935][    T1] iommu: Default domain type: Translated
[    0.536780][    T1] iommu: DMA domain TLB invalidation policy: lazy mode
[    0.550487][    T1] SCSI subsystem initialized
[    0.556842][    T1] usbcore: registered new interface driver usbfs
[    0.563561][    T1] usbcore: registered new interface driver hub
[    0.570059][    T1] usbcore: registered new device driver usb
[    0.577021][    T1] pps_core: LinuxPPS API ver. 1 registered
[    0.582956][    T1] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.593318][    T1] PTP clock support registered
[    0.599527][    T1] EDAC MC: Ver: 3.0.0
[    0.608594][    T1] NetLabel: Initializing
[    0.612924][    T1] NetLabel:  domain hash size = 128
[    0.618238][    T1] NetLabel:  protocols = UNLABELED CIPSOv4 CALIPSO
[    0.625216][    T1] NetLabel:  unlabeled traffic allowed by default
[    0.632416][    T1] vgaarb: loaded
[    0.640220][    T1] clocksource: Switched to clocksource riscv_clocksource
[    1.598407][    T1] VFS: Disk quotas dquot_6.6.0
[    1.603734][    T1] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    1.645592][    T1] NET: Registered PF_INET protocol family
[    1.652057][    T1] IP idents hash table entries: 131072 (order: 8, 1048576 bytes, linear)
[    1.677107][    T1] tcp_listen_portaddr_hash hash table entries: 4096 (order: 4, 65536 bytes, linear)
[    1.686923][    T1] Table-perturb hash table entries: 65536 (order: 6, 262144 bytes, linear)
[    1.696138][    T1] TCP established hash table entries: 65536 (order: 7, 524288 bytes, linear)
[    1.705717][    T1] TCP bind hash table entries: 65536 (order: 9, 2097152 bytes, linear)
[    1.716967][    T1] TCP: Hash tables configured (established 65536 bind 65536)
[    1.724964][    T1] UDP hash table entries: 4096 (order: 5, 131072 bytes, linear)
[    1.732921][    T1] UDP-Lite hash table entries: 4096 (order: 5, 131072 bytes, linear)
[    1.742011][    T1] NET: Registered PF_UNIX/PF_LOCAL protocol family
[    1.751026][    T1] RPC: Registered named UNIX socket transport module.
[    1.758070][    T1] RPC: Registered udp transport module.
[    1.763760][    T1] RPC: Registered tcp transport module.
[    1.769450][    T1] RPC: Registered tcp-with-tls transport module.
[    1.775967][    T1] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    1.783459][    T1] PCI: CLS 0 bytes, default 64
[    1.794524][    T1] Initialise system trusted keyrings
[    1.800120][    T1] Key type blacklist registered
[    1.805762][    T1] workingset: timestamp_bits=39 max_order=21 bucket_order=0
[    1.813451][    T1] zbud: loaded
[    1.822873][    T1] NFS: Registering the id_resolver key type
[    1.829086][    T1] Key type id_resolver registered
[    1.834208][    T1] Key type id_legacy registered
[    1.839319][    T1] nfs4filelayout_init: NFSv4 File Layout Driver Registering...
[    1.847653][    T1] 9p: Installing v9fs 9p2000 file system support
[    1.858020][    T1] integrity: Platform Keyring initialized
[    1.932739][    T1] NET: Registered PF_ALG protocol family
[    1.938565][    T1] Key type asymmetric registered
[    1.943615][    T1] Asymmetric key parser 'x509' registered
[    1.949814][    T1] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 245)
[    1.958861][    T1] io scheduler mq-deadline registered
[    1.964389][    T1] io scheduler kyber registered
[    1.969501][    T1] io scheduler bfq registered
[    2.017409][    T1] shpchp: Standard Hot Plug PCI Controller Driver version: 0.4
[    2.061155][    T1] light-fm-clk ffef010000.clock-controller: succeed to register light fullmask clock driver
[    2.075348][    T1] dw_axi_dmac_platform ffefc00000.dma-controller: DesignWare AXI DMA Controller, 4 channels
[    2.091256][    T1] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    2.106809][    T1] printk: console [ttyS0] disabled
[    2.113570][    T1] ffe7014000.serial: ttyS0 at MMIO 0xffe7014000 (irq = 19, base_baud = 6250000) is a 16550A
[    2.124093][    T1] printk: console [ttyS0] enabled
[    2.124093][    T1] printk: console [ttyS0] enabled
[    2.134121][    T1] printk: bootconsole [uart0] disabled
[    2.134121][    T1] printk: bootconsole [uart0] disabled
[    2.153814][    T1] loop: module loaded
[    2.157998][    T1] rdac: device handler registered
[    2.163563][    T1] hp_sw: device handler registered
[    2.168616][    T1] emc: device handler registered
[    2.173947][    T1] alua: device handler registered
[    2.189233][    T1] e1000e: Intel(R) PRO/1000 Network Driver
[    2.194959][    T1] e1000e: Copyright(c) 1999 - 2015 Intel Corporation.
[    2.205212][    T1] usbcore: registered new interface driver uas
[    2.211442][    T1] usbcore: registered new interface driver usb-storage
[    2.219069][    T1] mousedev: PS/2 mouse device common for all mice
[    2.226527][    T1] i2c_dev: i2c /dev entries driver
[    2.233943][    T1] Watchdog module: light-wdt loaded
[    2.240448][    T1] sdhci: Secure Digital Host Controller Interface driver
[    2.247368][    T1] sdhci: Copyright(c) Pierre Ossman
[    2.252597][    T1] sdhci-pltfm: SDHCI platform and OF driver helper
[    2.261487][    T1] hid: raw HID events driver (C) Jiri Kosina
[    2.270153][    T1] usbcore: registered new interface driver usbhid
[    2.276566][    T1] usbhid: USB HID core driver
[    2.284007][    T1] thead,light-mbox-client soc:mbox_910t_client2: Successfully registered
[    2.293446][    T1] riscv-pmu-sbi: SBI PMU extension is available
[    2.299652][   T76] mmc0: SDHCI controller on ffe7080000.mmc [ffe7080000.mmc] using ADMA 64-bit
[    2.299917][    T1] riscv-pmu-sbi: 16 firmware and 18 hardware counters
[    2.320345][   T37] mmc1: SDHCI controller on ffe7090000.mmc [ffe7090000.mmc] using ADMA 64-bit
[    2.359355][   T75] mmc0: new HS400 MMC card at address 0001
[    2.368732][   T75] mmcblk0: mmc0:0001 Y2P032 29.1 GiB
[    2.388093][   T75]  mmcblk0: p1 p2 p3
[    2.396306][    T1] NET: Registered PF_INET6 protocol family
[    2.397244][   T75] mmcblk0boot0: mmc0:0001 Y2P032 4.00 MiB
[    2.406198][    T1] Segment Routing with IPv6
[    2.412517][    T1] In-situ OAM (IOAM) with IPv6
[    2.415146][   T75] mmcblk0boot1: mmc0:0001 Y2P032 4.00 MiB
[    2.417546][    T1] NET: Registered PF_PACKET protocol family
[    2.429021][   T75] mmcblk0rpmb: mmc0:0001 Y2P032 16.0 MiB, chardev (240:0)
[    2.429436][    T1] 9pnet: Installing 9P2000 support
[    2.441368][    T1] Key type dns_resolver registered
[    2.503515][    T1] registered taskstats version 1
[    2.511534][    T1] Loading compiled-in X.509 certificates
[    2.633650][    T1] Loaded X.509 cert 'Build time autogenerated kernel key: c08c05d287c9641db18bbf0d7a340c8803115f8d'
[    2.698100][    T1] kmemleak: Kernel memory leak detector initialized (mem pool available: 15673)
[    2.698117][  T102] kmemleak: Automatic memory scanning thread started
[    2.720614][    T1] Key type encrypted registered
[    2.725473][    T1] ima: No TPM chip found, activating TPM-bypass!
[    2.732117][    T1] ima: Allocated hash algorithm: sha256
[    2.737706][    T1] ima: No architecture policies found
[    2.743309][    T1] evm: Initialising EVM extended attributes:
[    2.749223][    T1] evm: security.selinux
[    2.753270][    T1] evm: security.SMACK64 (disabled)
[    2.758269][    T1] evm: security.SMACK64EXEC (disabled)
[    2.763612][    T1] evm: security.SMACK64TRANSMUTE (disabled)
[    2.769390][    T1] evm: security.SMACK64MMAP (disabled)
[    2.774732][    T1] evm: security.apparmor
[    2.778859][    T1] evm: security.ima
[    2.782552][    T1] evm: security.capability
[    2.786875][    T1] evm: HMAC attrs: 0x1
[    3.251779][   T37] pca953x 0-0018: supply vcc not found, using dummy regulator
[    3.259809][   T37] pca953x 0-0018: using no AI
[    3.270183][   T37] pca953x 1-0018: supply vcc not found, using dummy regulator
[    3.278126][   T37] pca953x 1-0018: using no AI
[    3.288756][   T37] pca953x 3-0018: supply vcc not found, using dummy regulator
[    3.296805][   T37] pca953x 3-0018: using no AI
[    3.306140][   T37] succeed to create power domain debugfs direntry
[    3.319382][   T83] soc_vdd_3v3_en GPIO handle specifies active low - ignored
[    3.324804][   T76] soc_lcd0_vdd33_en GPIO handle specifies active low - ignored
[    3.327949][   T83] soc_lcd0_vdd18_en GPIO handle specifies active low - ignored
[    3.330513][  T168] soc_vdd5v_se_en GPIO handle specifies active low - ignored
[    3.331721][  T168] soc_wcn33_en GPIO handle specifies active low - ignored
[    3.332944][  T168] soc_vbus_en GPIO handle specifies active low - ignored
[    3.332974][  T168] reg-fixed-voltage aon:soc_vbus_en: nonexclusive access to GPIO for (null)
[    3.334470][  T169] soc_dovdd18_rgb GPIO handle specifies active low - ignored
[    3.337611][   T76] soc_dvdd12_rgb GPIO handle specifies active low - ignored
[    3.340053][   T76] soc_avdd25_ir GPIO handle specifies active low - ignored
[    3.340083][   T76] reg-fixed-voltage aon:soc_avdd25_ir: nonexclusive access to GPIO for (null)
[    3.340460][  T168] soc_avdd28_rgb GPIO handle specifies active low - ignored
[    3.341458][   T76] soc_dovdd18_ir GPIO handle specifies active low - ignored
[    3.342629][  T170] soc_dvdd12_ir GPIO handle specifies active low - ignored
[    3.342650][  T168] soc_cam2_avdd25_ir GPIO handle specifies active low - ignored
[    3.343855][   T76] soc_cam2_dovdd18_ir GPIO handle specifies active low - ignored
[    3.343886][   T76] reg-fixed-voltage aon:soc_cam2_dovdd18_ir: nonexclusive access to GPIO for (null)
[    3.345069][   T76] soc_cam2_dvdd12_ir GPIO handle specifies active low - ignored
[    3.345105][   T76] reg-fixed-voltage aon:soc_cam2_dvdd12_ir: nonexclusive access to GPIO for (null)
[    3.489309][   T37] cpufreq: cpufreq_online: CPU0: Running at unlisted initial frequency: 750000 KHz, changing to: 800000 KHz
[    3.510944][   T37] cpu cpu0: finish to register cpufreq driver
[    3.517449][   T37] thead,light-aon-test aon:light-aon-test: Successfully registered
[    3.530960][   T37] light-event soc:light-event: failed to set aon reservemem
[    3.538150][   T37] light-event soc:light-event: set aon reservemem failed!
[    3.545138][   T37] light-event: probe of soc:light-event failed with error -1
[    3.554487][   T37] [light_wdt_probe,329] register power off callback
[    3.561049][   T37] succeed to register light pmic watchdog
[    3.568175][    T1] light rpmsg: Ready for cross core communication!
[    3.574568][    T1] light rpmsg: rproc_name = m4
[    4.104261][    T1] virtio_rpmsg_bus virtio0: rpmsg host is online
[    4.115321][    T1] light rpmsg: driver is registered.
[    4.121481][    T1] clk: Not disabling unused clocks
[    4.126824][    T1] dw-apb-uart ffe7014000.serial: forbid DMA for kernel console
[    4.134349][    T1] md: Waiting for all devices to be available before autodetect
[    4.141867][    T1] md: If you don't use raid, use raid=noautodetect
[    4.148243][    T1] md: Autodetecting RAID arrays.
[    4.153046][    T1] md: autorun ...
[    4.156544][    T1] md: ... autorun DONE.
[    4.176866][    T1] EXT4-fs (mmcblk0p3): mounted filesystem 51b60aff-2a51-4b05-8cd7-7551c231cae1 r/w with ordered data mode. Quota mode: none.
[    4.189809][    T1] VFS: Mounted root (ext4 filesystem) on device 179:3.
[    4.198047][    T1] devtmpfs: mounted
[    4.202341][    T1] integrity: Unable to open file: /etc/keys/x509_ima.der (-2)
[    4.202377][    T1] integrity: Unable to open file: /etc/keys/x509_evm.der (-2)
[    4.217853][    T1] Freeing unused kernel image (initmem) memory: 2756K
[    4.232316][    T1] Run /sbin/init as init process
[    4.425015][    T1] SELinux: https://github.com/SELinuxProject/selinux-kernel/wiki/DEPRECATE-runtime-disable
[    4.434900][    T1] SELinux: Runtime disable is not supported, use selinux=0 on the kernel cmdline.
[    4.450747][    T1] systemd[1]: System time before build time, advancing clock.
[    4.505366][    T1] systemd[1]: systemd v253-5.oe2309 running in system mode (+PAM +AUDIT +SELINUX -APPARMOR +IMA -SMACK +SECCOMP +GCRYPT +GNUTLS -OPENSSL +ACL +)
[    4.538545][    T1] systemd[1]: Detected architecture riscv64.

Welcome to openEuler 23.09!

[    4.622060][    T1] systemd[1]: Hostname set to <openeuler-riscv64>.
[    5.001564][    T1] systemd[1]: /usr/lib/systemd/system/dbus.socket:5: ListenStream= references a path below legacy directory /var/run/, updating /var/run/dbus/s.
[    5.249029][    T1] systemd[1]: Queued start job for default target Graphical Interface.
[    5.340644][    T1] systemd[1]: Created slice Slice /system/getty.
[  OK  ] Created slice Slice /system/getty.
[    5.367838][    T1] systemd[1]: Created slice Slice /system/modprobe.
[  OK  ] Created slice Slice /system/modprobe.
[    5.391726][    T1] systemd[1]: Created slice Slice /system/serial-getty.
[  OK  ] Created slice Slice /system/serial-getty.
[    5.419866][    T1] systemd[1]: Created slice Slice /system/sshd-keygen.
[  OK  ] Created slice Slice /system/sshd-keygen.
[    5.451444][    T1] systemd[1]: Created slice User and Session Slice.
[  OK  ] Created slice User and Session Slice.
[    5.472879][    T1] systemd[1]: Started Forward Password Requests to Wall Directory Watch.
[  OK  ] Started Forward Password R…uests to Wall Directory Watch.
[    5.501447][    T1] systemd[1]: Set up automount Arbitrary Executable File Formats File System Automount Point.
[  OK  ] Set up automount Arbitrary…s File System Automount Point.
[    5.528811][    T1] systemd[1]: Reached target Slice Units.
[  OK  ] Reached target Slice Units.
[    5.552450][    T1] systemd[1]: Reached target Swaps.
[  OK  ] Reached target Swaps.
[    5.572985][    T1] systemd[1]: Listening on Device-mapper event daemon FIFOs.
[  OK  ] Listening on Device-mapper event daemon FIFOs.
[    5.601286][    T1] systemd[1]: Listening on LVM2 poll daemon socket.
[  OK  ] Listening on LVM2 poll daemon socket.
[    5.629126][    T1] systemd[1]: Listening on RPCbind Server Activation Socket.
[  OK  ] Listening on RPCbind Server Activation Socket.
[    5.652403][    T1] systemd[1]: Reached target RPC Port Mapper.
[  OK  ] Reached target RPC Port Mapper.
[    5.679299][    T1] systemd[1]: Listening on Process Core Dump Socket.
[  OK  ] Listening on Process Core Dump Socket.
[    5.700904][    T1] systemd[1]: Listening on initctl Compatibility Named Pipe.
[  OK  ] Listening on initctl Compatibility Named Pipe.
[    5.725350][    T1] systemd[1]: Listening on Journal Socket (/dev/log).
[  OK  ] Listening on Journal Socket (/dev/log).
[    5.749379][    T1] systemd[1]: Listening on Journal Socket.
[  OK  ] Listening on Journal Socket.
[    5.775367][    T1] systemd[1]: Listening on udev Control Socket.
[  OK  ] Listening on udev Control Socket.
[    5.797110][    T1] systemd[1]: Listening on udev Kernel Socket.
[  OK  ] Listening on udev Kernel Socket.
[    5.844719][    T1] systemd[1]: Mounting Huge Pages File System...
         Mounting Huge Pages File System...
[    5.872271][    T1] systemd[1]: Mounting POSIX Message Queue File System...
         Mounting POSIX Message Queue File System...
[    5.924790][    T1] systemd[1]: Mounting Kernel Debug File System...
         Mounting Kernel Debug File System...
[    5.951871][    T1] systemd[1]: Mounting Kernel Trace File System...
         Mounting Kernel Trace File System...
[    5.983386][    T1] systemd[1]: Mounting Temporary Directory /tmp...
         Mounting Temporary Directory /tmp...
[    6.004715][    T1] systemd[1]: Kernel Module supporting RPCSEC_GSS was skipped because of an unmet condition check (ConditionPathExists=/etc/krb5.keytab).
[    6.027003][    T1] systemd[1]: Starting Create List of Static Device Nodes...
         Starting Create List of Static Device Nodes...
[    6.056339][    T1] systemd[1]: Starting Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling...
         Starting Monitoring of LVM…meventd or progress polling...
[    6.117308][    T1] systemd[1]: Starting Load Kernel Module configfs...
         Starting Load Kernel Module configfs...
[    6.148523][    T1] systemd[1]: Starting Load Kernel Module drm...
         Starting Load Kernel Module drm...
[    6.176514][    T1] systemd[1]: Starting Load Kernel Module fuse...
         Starting Load Kernel Module fuse...
[    6.197734][    T1] systemd[1]: systemd-journald.service: unit configures an IP firewall, but the local system does not support BPF/cgroup firewalling.
[    6.211469][    T1] systemd[1]: (This warning is only shown for the first unit using IP firewalling.)
[    6.230042][    T1] systemd[1]: Starting Journal Service...
         Starting Journal Service...
[    6.250598][    T1] systemd[1]: Load Kernel Modules was skipped because no trigger condition checks were met.
[    6.269178][    T1] systemd[1]: Starting Generate network units from Kernel command line...
         Starting Generate network …ts from Kernel command line...
[    6.288729][  T218] systemd-journald[218]: Collecting audit messages is disabled.
[    6.313142][    T1] systemd[1]: Starting Remount Root and Kernel File Systems...
         Starting Remount Root and Kernel File Systems...
[    6.349116][    T1] systemd[1]: Starting Apply Kernel Variables...
         Starting Apply Kernel Variables...
[    6.386069][    T1] systemd[1]: Starting Coldplug All udev Devices...
         Starting Coldplug All udev Devices...
[    6.412244][  T217] fuse: init (API version 7.39)
[    6.419096][    T1] systemd[1]: Starting Setup Virtual Console...
         Starting Setup Virtual Console...
[    6.461309][    T1] systemd[1]: Started Journal Service.
[  OK  ] Started Journal Service.
[  OK  ] Mounted Huge Pages File System.
[  OK  ] Mounted POSIX Message Queue File System.
[  OK  ] Mounted Kernel Debug File System.
[  OK  ] Mounted Kernel Trace File System.
[  OK  ] Mounted Temporary Directory /tmp.
[  OK  ] Finished Create List of Static Device Nodes.
[  OK  ] Finished Monitoring of LVM… dmeventd or progress polling.
[  OK  ] Finished Load Kernel Module configfs.
1;-1f[  OK  ] Started Show Plymouth Boot Screen.
[  OK  ] Started Forward Password R…s to Plymouth Directory Watch.
[  OK  ] Reached target Path Units.
[  OK  ] Found device /dev/ttyS0.
[  OK  ] Found device /dev/mmcblk0p2.
         Mounting /boot...
[    8.213602][  T277] hwmon hwmon1: temp2_input not attached to any thermal zone
[    8.371471][  T282] EXT4-fs (mmcblk0p2): mounted filesystem 04b7aa33-732b-4984-afb9-8116b2749aa8 r/w with ordered data mode. Quota mode: none.
[  OK  ] Mounted /boot.
[  OK  ] Reached target Local File Systems.
[    8.692411][  T254] thead-dwmac ffe7070000.ethernet: IRQ eth_wake_irq not found
[    8.699775][  T254] thead-dwmac ffe7070000.ethernet: IRQ eth_lpi not found
[    8.706956][  T254] thead-dwmac ffe7070000.ethernet: PTP uses main clock
[    8.714523][  T254] thead-dwmac ffe7070000.ethernet: User ID: 0x10, Synopsys ID: 0x37
[    8.722439][  T254] thead-dwmac ffe7070000.ethernet:         DWMAC1000
[    8.728417][  T254] thead-dwmac ffe7070000.ethernet: DMA HW capability register supported
[    8.736629][  T254] thead-dwmac ffe7070000.ethernet: RX Checksum Offload Engine supported
[    8.744936][  T254] thead-dwmac ffe7070000.ethernet: COE Type 2
[    8.750926][  T254] thead-dwmac ffe7070000.ethernet: TX Checksum insertion supported
[    8.758730][  T254] thead-dwmac ffe7070000.ethernet: Enhanced/Alternate descriptors
[    8.758738][  T254] thead-dwmac ffe7070000.ethernet: Enabled extended descriptors
[    8.758742][  T254] thead-dwmac ffe7070000.ethernet: Ring mode enabled
[    8.758746][  T254] thead-dwmac ffe7070000.ethernet: Enable RX Mitigation via HW Watchdog Timer
[    8.787094][  T268] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
[    8.798903][  T268] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 1
[    8.809520][  T268] xhci-hcd xhci-hcd.0.auto: hcc params 0x0220fe65 hci version 0x110 quirks 0x0000008000000810
         Startin[    8.820267][  T268] xhci-hcd xhci-hcd.0.auto: irq 28, io mem 0xffe7040000
g Tell Plymouth To Writ[    8.830054][  T268] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
e Out Runtime Da[    8.836238][  T254] thead-dwmac ffe7060000.ethernet: IRQ eth_wake_irq not found
ta...
[    8.837559][  T268] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 2
[    8.845875][  T254] thead-dwmac ffe7060000.ethernet: IRQ eth_lpi not found
[    8.846498][  T254] thead-dwmac ffe7060000.ethernet: PTP uses main clock
[    8.855692][  T268] xhci-hcd xhci-hcd.0.auto: Host supports USB 3.0 SuperSpeed
[    8.868788][  T254] thead-dwmac ffe7060000.ethernet: User ID: 0x10, Synopsys ID: 0x37
[    8.870648][  T268] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 6.06
[    8.876519][  T254] thead-dwmac ffe7060000.ethernet:         DWMAC1000
[    8.884373][  T268] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    8.884384][  T268] usb usb1: Product: xHCI Host Controller
[    8.884390][  T268] usb usb1: Manufacturer: Linux 6.6.0-44 xhci-hcd
[    8.884394][  T268] usb usb1: SerialNumber: xhci-hcd.0.auto
[    8.885685][  T268] hub 1-0:1.0: USB hub found
[    8.893519][  T254] thead-dwmac ffe7060000.ethernet: DMA HW capability register supported
[    8.893527][  T254] thead-dwmac ffe7060000.ethernet: RX Checksum Offload Engine supported
[    8.893531][  T254] thead-dwmac ffe7060000.ethernet: COE Type 2
[    8.899557][  T268] hub 1-0:1.0: 1 port detected
[    8.907444][  T254] thead-dwmac ffe7060000.ethernet: TX Checksum insertion supported
[    8.915149][  T268] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 6.06
[    8.919398][  T254] thead-dwmac ffe7060000.ethernet: Enhanced/Alternate descriptors
[    8.924981][  T268] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    8.924992][  T268] usb usb2: Product: xHCI Host Controller
[    8.924997][  T268] usb usb2: Manufacturer: Linux 6.6.0-44 xhci-hcd
[    8.925001][  T268] usb usb2: SerialNumber: xhci-hcd.0.auto
[    8.929487][  T254] thead-dwmac ffe7060000.ethernet: Enabled extended descriptors
[    8.938882][  T268] hub 2-0:1.0: USB hub found
[    8.945901][  T254] thead-dwmac ffe7060000.ethernet: Ring mode enabled
[    8.951917][  T268] hub 2-0:1.0: 1 port detected
[    8.956516][  T254] thead-dwmac ffe7060000.ethernet: Enable RX Mitigation via HW Watchdog Timer
         Starting Create Volatile Files and Directories...
[  OK  ] Finished Tell Plymouth To Write Out Runtime Data.
[    9.146196][  T255] thead-dwmac ffe7060000.ethernet end1: renamed from eth1
[    9.180306][   T74] usb 1-1: new high-speed USB device number 2 using xhci-hcd
[  OK  ] Finished Create Volatile Files and Directories.
[    9.221548][  T265] thead-dwmac ffe7070000.ethernet end0: renamed from eth0
         Mounting RPC Pipe File System...
         Starting Security Auditing Service...
         Starting MD array monitor...
         Starting RPC Bind...
[  OK  ] Mounted RPC Pipe File System.
[    9.339846][   T74] usb 1-1: New USB device found, idVendor=2109, idProduct=2817, bcdDevice= 7.00
[    9.348863][   T74] usb 1-1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[    9.356807][   T74] usb 1-1: Product: USB2.0 Hub             
[    9.362660][   T74] usb 1-1: Manufacturer: VIA Labs, Inc.         
[  OK  ] Reached target rpc_pipefs.target.
[    9.413955][   T74] hub 1-1:1.0: USB hub found
[    9.422050][   T74] hub 1-1:1.0: 5 ports detected
[FAILED] Failed to start MD array monitor.
See 'systemctl status mdmonitor.service' for details.
[    9.477506][   T75] usb 2-1: new SuperSpeed USB device number 2 using xhci-hcd
[  OK  ] Started RPC Bind.
[  OK  ] Started Security Auditing Service.
[    9.642744][   T75] usb 2-1: New USB device found, idVendor=2109, idProduct=0817, bcdDevice= 7.00
[    9.651746][   T75] usb 2-1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[    9.651757][   T75] usb 2-1: Product: USB3.0 Hub             
[    9.651763][   T75] usb 2-1: Manufacturer: VIA Labs, Inc.         
         Starting Record System Boot/Shutdown in UTMP...
[  OK  ] Finished Record System Boot/Shutdown in UTMP.
[  OK  ] Reached target System Initialization.
[  OK  ] Started dnf makecache --timer.
[  OK  ] Started Daily Cleanup of Temporary Directories.
[  OK  ] Reached targe[    9.717855][   T75] hub 2-1:1.0: USB hub found
t Timer[    9.723835][   T75] hub 2-1:1.0: 4 ports detected
 Units.
[  OK  ] Listening on D-Bus System Message Bus Socket.
[  OK  ] Listening on PC/SC Smart Card Daemon Activation Socket.
[  OK  ] Reached target Socket Units.
[  OK  ] Reached target Basic System.
         Starting NTP client/server...
         Starting D-Bus System Message Bus...
         Starting Restore /run/initramfs on shutdown...
         Starting Update RTC With System Clock...
         Starting irqbalance daemon...
[  OK  ] Started libstoragemgmt plug-in server daemon.
         Starting Authorization Manager...
[  OK  ] Started resize rootfs.
[  OK  ] Started Hardware RNG Entropy Gatherer Daemon.
[    9.845267][  T281] usbcore: registered new device driver onboard-usb-hub
         Starting Self Monitoring a…g Technology (SMART) Daemon...
[  OK  ] Reached target sshd-keygen.target.
         Starting User Login Management...
         Starting Run a configured … scripts at system startup....
[  OK  ] Finished Restore /run/initramfs on shutdown.
[  OK  ] Finished Update RTC With System Clock.
[  OK  ] Started irqbalance daemon.
[  OK  ] Started D-Bus System Message Bus.
[   10.126412][  T281] hub 1-1:1.0: USB hub found

[   10.140988][  T281] hub 1-1:1.0: 5 ports detected
         Starting firewalld - dynamic firewall daemon...
[  OK  ] Started Self Monitoring an…ing Technology (SMART) Daemon.
[  OK  ] Started NTP client/server.
[  OK  ] Started User Login Management.
[  OK  ] Finished Run a configured …ap scripts at system startup..
[   10.420690][  T170] hub 1-1:1.0: USB hub found
[   10.434422][  T170] hub 1-1:1.0: 5 ports detected
[   10.764620][  T170] hub 1-1:1.0: USB hub found
[   10.771695][  T170] hub 1-1:1.0: 5 ports detected
[   10.976600][  T281] hub 2-1:1.0: USB hub found
[   10.984352][  T281] hub 2-1:1.0: 4 ports detected
[   11.124672][  T170] hub 1-1:1.0: USB hub found
[   11.132296][  T170] hub 1-1:1.0: 5 ports detected
[   11.428277][  T170] hub 2-1:1.0: USB hub found
[   11.435581][  T170] hub 2-1:1.0: 4 ports detected
[   11.788215][  T234] usb 1-1.5: new high-speed USB device number 3 using xhci-hcd
[   11.944439][  T234] usb 1-1.5: New USB device found, idVendor=2109, idProduct=8817, bcdDevice= 0.01
[   11.953565][  T234] usb 1-1.5: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[   11.961642][  T234] usb 1-1.5: Product: USB Billboard Device   
[   11.967635][  T234] usb 1-1.5: Manufacturer: VIA Labs, Inc.         
[   11.974074][  T234] usb 1-1.5: SerialNumber: 0000000000000001
[  OK  ] Started firewalld - dynamic firewall daemon.
[  OK  ] Reached target Preparation for Network.
         Starting Network Manager...
[  OK  ] Started Network Manager.
[  OK  ] Reached target Network.
         Starting Network Manager Wait Online...
         Starting GSSAPI Proxy Daemon...
         Starting /etc/rc.d/rc.local Compatibility...
         Starting OpenSSH server daemon...
         Starting Dynamic System Tuning Daemon...
[  OK  ] Started /etc/rc.d/rc.local Compatibility.
         Starting Hostname Service...
[  OK  ] Started OpenSSH server daemon.
[  OK  ] Started GSSAPI Proxy Daemon.
[  OK  ] Reached target NFS client services.
[  OK  ] Reached target Preparation for Remote File Systems.
[  OK  ] Reached target Remote File Systems.
         Starting Permit User Sessions...
[  OK  ] Finished Permit User Sessions.
[  OK  ] Started Deferred execution scheduler.
[  OK  ] Started Command Scheduler.
         Starting Hold until boot process finishes up...
         Starting Terminate Plymouth Boot Screen...
[   17.370034][  T396] thead-dwmac ffe7070000.ethernet end0: Register MEM_TYPE_PAGE_POOL RxQ-0
[   17.560139][  T396] thead-dwmac ffe7070000.ethernet end0: PHY [stmmac-0:01] driver [RTL8211F Gigabit Ethernet] (irq=POLL)
[   17.585473][  T396] dwmac1000: Master AXI performs fixed burst length
[   17.592069][  T396] thead-dwmac ffe7070000.ethernet end0: No Safety Features support found
[   17.797768][  T396] thead-dwmac ffe7070000.ethernet end0: IEEE 1588-2008 Advanced Timestamp supported
[   18.028343][  T396] thead-dwmac ffe7070000.ethernet end0: registered PTP clock
[   18.064080][  T396] thead-dwmac ffe7070000.ethernet end0: configuring for phy/rgmii-id link mode
[   18.198914][  T396] thead-dwmac ffe7060000.ethernet end1: Register MEM_TYPE_PAGE_POOL RxQ-0
[   18.408463][  T396] thead-dwmac ffe7060000.ethernet end1: PHY [stmmac-0:02] driver [RTL8211F Gigabit Ethernet] (irq=POLL)
[   18.440151][  T396] dwmac1000: Master AXI performs fixed burst length
[   18.446686][  T396] thead-dwmac ffe7060000.ethernet end1: No Safety Features support found
[   18.652237][  T396] thead-dwmac ffe7060000.ethernet end1: IEEE 1588-2008 Advanced Timestamp supported
[   18.772626][  T396] thead-dwmac ffe7060000.ethernet end1: registered PTP clock
[   18.797793][  T396] thead-dwmac ffe7060000.ethernet end1: configuring for phy/rgmii-id link mode
[   21.012217][   T58] thead-dwmac ffe7060000.ethernet end1: Link is Up - 100Mbps/Full - flow control rx/tx
[   21.719741][  T449] fbcon: Taking over console


Authorized users only. All activities may be monitored and reported.
openeuler-riscv64 login:
